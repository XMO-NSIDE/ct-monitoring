///$tab ### Result-F_Enrollment ###
/* ========================================================================================
   Patient Data - Enrollement - Recruitment - Randomization
   This fact is useful to know how many patients are randomized and compare that with the forecast
   ======================================================================================== */

 
/* ---------------------------------------------------------------------------------------------
   Load extract data for the enrollment
   --------------------------------------------------------------------------------------------- */
NoConcatenate
[F_Enrollment]:
	LOAD
		'#Result'																					 AS [%Fact],
		'#Enrollment'																				 AS [%Fact section],
		'#Patients in extract'									 AS [%Fact subsection],
        "monitoring_dashboard_preprocessed_data-extract_aggregated_patient-id" as [Patient ID],
    	ApplyMap('map_Preprocessed-data-tag_To_Study-ID'
 			,"monitoring_dashboard_preprocessed_data-extract_aggregated_patient-__tag")				 AS [%L_Study],
    	"monitoring_dashboard_preprocessed_data-extract_aggregated_patient-status"					 AS [status],
    	Date("monitoring_dashboard_preprocessed_data-extract_aggregated_patient-randomization_date"
    		, 'DD-MMM-YYYY') 																		 AS [Enrollment date],
    	Num#("monitoring_dashboard_preprocessed_data-extract_aggregated_patient-number_of_patients") AS [Number of patients]
	FROM [$(dataDirectory)/monitoring_dashboard_preprocessed_data-extract_aggregated_patient.csv]
	(txt, codepage is 28591, embedded labels, delimiter is ',', msq);

//CT-3641 add a last row corresponding to the extract date in order to visualize on the chart data until 
//the last point in time even if there are no new patients randomized until the extract date
/*Last_enrollment_tmp:
First 1
LOAD
[Enrollment date] //31-Jul-2020 (date format)
RESIDENT [F_Enrollment]
order by [Enrollment date] desc;

LET vLastEnrollment = peek('Enrollment date',0);
drop table Last_enrollment_tmp;

LET vLastEnrollmentExtract = makedate(subfield('$(ExtractDate)','-',1),subfield('$(ExtractDate)','-',2),subfield('$(ExtractDate)','-',3));

IF vLastEnrollment <> vLastEnrollmentExtract ;
First 1
LOAD
[%Fact],
[%Fact section],
[%Fact subsection],
'' as [Patient ID],
'' as [%L_Study],
'' as [status],
''as [Number of patients],

'$(vLastEnrollmentExtract)' as [Enrollment date] //31-Jul-2020 (date format)

RESIDENT [F_Enrollment]
order by [Enrollment date] desc;

END IF;
*/

//CT-4754 definition of a new global table - not impacted by the multi suppliers local recruitment data
//as the row are duplicated (left join statement below) by the number of suppliers for each local site
[F_Enrollment_Global]:
NoConcatenate 
LOAD *
RESIDENT [F_Enrollment];

if FileSize('[$(dataDirectory)/monitoring_dashboard_preprocessed_data-extract_aggregated_patient-suppliers.csv]')>0 then // This table is not always part of the dataset. With this condition the loading is not interrupted if the table doesn't exist. 
LEFT Join ([F_Enrollment])
[F_Suppliers]:
LOAD 
"monitoring_dashboard_preprocessed_data-extract_aggregated_patient-id"  as [Patient ID],
"monitoring_dashboard_preprocessed_data-extract_aggregated_patient-suppliers" AS [Warehouse ID]
FROM [$(dataDirectory)/monitoring_dashboard_preprocessed_data-extract_aggregated_patient-suppliers.csv]
	(txt, codepage is 28591, embedded labels, delimiter is ',', msq);
   END IF;


/* ---------------------------------------------------------------------------------------------
   This step is done in order to have a continuous calendar (all enrollment dates filled in the table)
   --------------------------------------------------------------------------------------------- */
 NoConcatenate
[tmp_Enrollment_dates]:
 LOAD DISTINCT
 	[Enrollment date]
 RESIDENT [F_Enrollment]
;

 OUTER JOIN([tmp_Enrollment_dates])
 LOAD DISTINCT
    Date("monitoring_dashboard_preprocessed_data-result_enrollment-date"
    		, 'DD-MMM-YYYY')				AS [Enrollment date (Forecast)]
 FROM [$(dataDirectory)/monitoring_dashboard_preprocessed_data-result_enrollment.csv]
 (txt, codepage is 28591, embedded labels, delimiter is ',', msq)
 WHERE [monitoring_dashboard_preprocessed_data-result_enrollment-measure]='Randomized'
 ;

 OUTER JOIN([tmp_Enrollment_dates])
 LOAD DISTINCT
    "monitoring_dashboard_preprocessed_data-info-extract_date"  AS [Extract date]
 FROM [$(dataDirectory)/monitoring_dashboard_preprocessed_data-info.csv]
 (txt, codepage is 28591, embedded labels, delimiter is ',', msq)
 ;

 LEFT JOIN([tmp_Enrollment_dates])
 LOAD
 	alt(minEDateExtract,minEDateForecast) as minEDate,
	alt(maxEDateExtract,maxEDateForecast) as maxEDate,
 ;
 LOAD DISTINCT
     Min(date([Enrollment date], 'DD-MMM-YYYY')) as minEDateExtract,
     Min(date([Enrollment date (Forecast)], 'DD-MMM-YYYY')) as minEDateForecast,
     Max(date([Enrollment date], 'DD-MMM-YYYY')) as maxEDateExtract,
     Max(date([Enrollment date (Forecast)], 'DD-MMM-YYYY')) as maxEDateForecast
 RESIDENT [tmp_Enrollment_dates];
 

Let vMinEDate = Num(Peek('minEDate', 0, 'tmp_Enrollment_dates'));
Let vMaxEDate = Num(Peek('maxEDate', 0, 'tmp_Enrollment_dates'));

TRACE $(vMinEDate);
TRACE $(vMaxEDate);


DROP TABLE [tmp_Enrollment_dates];

 NoConcatenate
	[tmp_Enrollment_calendar]:
  LOAD
	'#Result'											AS [%Fact],
	'#Enrollment'										AS [%Fact section],
	'#Patients in extract'								AS [%Fact subsection],
     Date($(vMinEDate) + IterNo() -1 ) 					AS [Enrollment date]
  AUTOGENERATE 1 
//CT-3641 need to until the extract date f
      	WHILE $(vMinEDate) + IterNo() -1 <= Num('$(ExtractDate)'); 
//    	WHILE $(vMinEDate) + IterNo() -1 <= Num('$(vMaxEDate)'); 

	OUTER JOIN([tmp_Enrollment_calendar])
	[tmp_Enrollment_keys]:
    LOAD DISTINCT 
		'#Result'										AS [%Fact],
		'#Enrollment'									AS [%Fact section],
		'#Patients in extract'							AS [%Fact subsection],
       [Warehouse ID],    
        [status]
    RESIDENT [F_Enrollment];
    
    

/* ---------------------------------------------------------------------------------------------
   This step is useful to link this table with the Fact Enrollment
   --------------------------------------------------------------------------------------------- */
RIGHT JOIN([F_Enrollment])
 LOAD * RESIDENT [tmp_Enrollment_calendar];
 
//CT-4754 apply same calendar data transformation to the Global randomization table
 NoConcatenate
	[tmp_Enrollment_calendar_Global]:
  LOAD
	'#Result'											AS [%Fact],
	'#Enrollment'										AS [%Fact section],
	'#Patients in extract'								AS [%Fact subsection],
     Date($(vMinEDate) + IterNo() -1 ) 					AS [Enrollment date]
  AUTOGENERATE 1 
    	WHILE $(vMinEDate) + IterNo() -1 <= Num('$(ExtractDate)'); 
     //   WHILE $(vMinEDate) + IterNo() -1  <= Num('$(vMaxEDate)'); 
 
RIGHT JOIN([F_Enrollment_Global])
 LOAD * RESIDENT [tmp_Enrollment_calendar_Global];
 
 drop table [tmp_Enrollment_calendar_Global];