# Qlik Application Repository 
# monitoring_default
### 
Created By XMO-NSIDE(Xavier Morosini) at Mon Apr 26 2021 16:51:00 GMT+0200 (Central European Summer Time)




Sheet Title | Description
------------ | -------------
Local dispensing|
Per depot randomization|
Depot shipments plan follow-up|
Inventories in extract|
Welcome|
Global randomization|
Clinical forecast accuracy overview|
Global dispensing|
Glossary|



Branch Name|Qlik application
---|---
master|[https://qlik-dev-feb19.n-side.com/sense/app/47d9b58b-9f1a-42fb-b391-25c4f2464e62](https://qlik-dev-feb19.n-side.com/sense/app/47d9b58b-9f1a-42fb-b391-25c4f2464e62)
develop|[https://qlik-dev-feb19.n-side.com/sense/app/d13b4769-6730-4b37-b992-485c28138433](https://qlik-dev-feb19.n-side.com/sense/app/d13b4769-6730-4b37-b992-485c28138433)
advanced-users|[https://qlik-dev-feb19.n-side.com/sense/app/7d0c874e-ccfb-4c78-8194-bc91be35d7f6](https://qlik-dev-feb19.n-side.com/sense/app/7d0c874e-ccfb-4c78-8194-bc91be35d7f6)